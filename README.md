# jfast

#### 介绍
JFast是一套轻量级的快速开发平台，集业务建模和代码生成于一身的基础框架，按照框架规范，可快速生成一套演示Demo系统，稍微改造即可变成一套可交付实施的软件系统。

#### 软件架构
本平台的核心采用JFinal+LayUI+MySQL的组合，轻量高效，扩展性强，适合大多数据业务场景。架构方面，平台采用前后端分离的方式，将所有的业务功能全部以标准接口的方式对外提供。


#### 安装教程

1.  安装JDK,MySQL，Eclipse
2.  打开eclipse，导入本系统代码
3.  依赖jar包，请在发行版中下载

#### 使用说明

请参考《开发说明文档》

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
