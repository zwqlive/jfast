/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50529
Source Host           : localhost:3306
Source Database       : java_fast

Target Server Type    : MYSQL
Target Server Version : 50529
File Encoding         : 65001

Date: 2020-05-24 09:35:59
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for dev_chart
-- ----------------------------
DROP TABLE IF EXISTS `dev_chart`;
CREATE TABLE `dev_chart` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `cate_id` int(11) NOT NULL COMMENT '分类',
  `code` varchar(50) NOT NULL COMMENT '代码',
  `title` varchar(100) NOT NULL COMMENT '名称',
  `ds_id` int(11) NOT NULL COMMENT '数据源',
  `type` varchar(20) NOT NULL COMMENT '类型',
  `comment` varchar(200) DEFAULT NULL COMMENT '说明',
  `list_sort` int(11) NOT NULL COMMENT '排序',
  `status` char(1) DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dev_chart
-- ----------------------------
INSERT INTO `dev_chart` VALUES ('1', '1', 'student_depart', '按部门统计学生', '3', 'pie', '按部门统计学生', '1', '1');
INSERT INTO `dev_chart` VALUES ('2', '1', 'student_depart2', '学生部门排名', '3', 'bar', '', '2', '1');
INSERT INTO `dev_chart` VALUES ('3', '1', 'student_depart3', '学生部门拆线', '3', 'line', '学生部门拆线', '4', '1');
INSERT INTO `dev_chart` VALUES ('4', '1', 'student_depart4', '学生表格', '3', 'table', '学生表格', '5', '1');

-- ----------------------------
-- Table structure for dev_dataset
-- ----------------------------
DROP TABLE IF EXISTS `dev_dataset`;
CREATE TABLE `dev_dataset` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `cate_id` int(11) NOT NULL COMMENT '分类',
  `code` varchar(50) NOT NULL COMMENT '部门代码',
  `title` varchar(100) NOT NULL COMMENT '部门名称',
  `sqls` text NOT NULL COMMENT '查询SQL',
  `list_sort` int(11) NOT NULL COMMENT '排序',
  `comment` varchar(200) DEFAULT NULL COMMENT '说明',
  `status` char(1) DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dev_dataset
-- ----------------------------
INSERT INTO `dev_dataset` VALUES ('2', '1', 'student_in_school', '在校生', 'select u.nickname,d.title from sys_user u\nleft join sys_department d on u.depart_id=d.id\n#for(x : cond) \n  #(for.first ? \"WHERE\": \"AND\") #(x.key) #para(x.value) \n #end', '1', 'select', '1');
INSERT INTO `dev_dataset` VALUES ('3', '1', 'student_depart', '按部门统计学生', 'select count(u.id) value,d.title title \nfrom sys_user u\nleft join sys_department d on u.depart_id=d.id\ngroup by depart_id\n#for(x : cond) \n  #(for.first ? \"WHERE\": \"AND\") #(x.key) #para(x.value) \n #end', '3', '按部门统计学生', '1');

-- ----------------------------
-- Table structure for dev_field
-- ----------------------------
DROP TABLE IF EXISTS `dev_field`;
CREATE TABLE `dev_field` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `model_id` int(11) NOT NULL COMMENT '所属模型',
  `code` varchar(50) NOT NULL COMMENT '代码',
  `title` varchar(50) NOT NULL COMMENT '名称',
  `db_type` varchar(10) NOT NULL COMMENT '数据库类型',
  `type` varchar(10) NOT NULL COMMENT '业务类型',
  `length` int(6) NOT NULL COMMENT '长度',
  `scale` int(2) DEFAULT '0' COMMENT '缩放',
  `nullable` char(1) NOT NULL COMMENT '空值',
  `defaults` varchar(100) DEFAULT NULL COMMENT '默认值',
  `calculate` varchar(200) DEFAULT NULL COMMENT '自动计算公式',
  `param` varchar(200) DEFAULT NULL COMMENT '参数',
  `list_sort` int(11) NOT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=180 DEFAULT CHARSET=utf8 COMMENT='字段';

-- ----------------------------
-- Records of dev_field
-- ----------------------------
INSERT INTO `dev_field` VALUES ('1', '2', 'module_id', '所属模块', 'number', 'number', '11', '0', '0', '', null, '', '1');
INSERT INTO `dev_field` VALUES ('2', '2', 'tables', '数据表', 'varchar', 'string', '100', '0', '0', '', null, '', '2');
INSERT INTO `dev_field` VALUES ('3', '3', 'model_id', '所属模型', 'number', 'number', '11', '0', '0', '', null, '', '1');
INSERT INTO `dev_field` VALUES ('4', '3', 'code', '代码', 'varchar', 'string', '20', '0', '0', '', null, '', '2');
INSERT INTO `dev_field` VALUES ('5', '3', 'title', '名称', 'varchar', 'string', '50', '0', '0', '', null, '', '3');
INSERT INTO `dev_field` VALUES ('6', '3', 'db_type', '数据库类型', 'varchar', 'string', '10', '0', '0', 'varchar', null, 'db_type', '4');
INSERT INTO `dev_field` VALUES ('7', '3', 'type', '业务类型', 'varchar', 'string', '10', '0', '0', 'string', null, 'field_type', '5');
INSERT INTO `dev_field` VALUES ('8', '3', 'length', '长度', 'number', 'number', '6', '0', '0', '10', null, '', '6');
INSERT INTO `dev_field` VALUES ('9', '3', 'scale', '缩放', 'number', 'number', '2', '0', '1', '0', null, '', '7');
INSERT INTO `dev_field` VALUES ('10', '3', 'nullable', '空值', 'varchar', 'string', '1', '0', '0', '0', null, '', '8');
INSERT INTO `dev_field` VALUES ('11', '3', 'defaults', '默认值', 'varchar', 'string', '100', '0', '1', '', null, '', '9');
INSERT INTO `dev_field` VALUES ('12', '3', 'calculate', '自动计算公式', 'varchar', 'string', '200', '0', '1', '', null, '', '10');
INSERT INTO `dev_field` VALUES ('13', '3', 'param', '参数', 'varchar', 'string', '200', '0', '1', '', null, '', '11');
INSERT INTO `dev_field` VALUES ('14', '3', 'list_sort', '排序', 'number', 'number', '11', '0', '0', '9', null, '', '12');
INSERT INTO `dev_field` VALUES ('15', '4', 'title', '规则名称', 'varchar', 'string', '50', '0', '0', '', '', '', '1');
INSERT INTO `dev_field` VALUES ('16', '4', 'model_id', '所属模型', 'varchar', 'relation', '11', '0', '0', '', '', 'dev_models', '2');
INSERT INTO `dev_field` VALUES ('17', '4', 'field_id', '字段', 'varchar', 'relation', '11', '0', '0', '', '', 'dev_field', '3');
INSERT INTO `dev_field` VALUES ('18', '4', 'type', '验证类型', 'varchar', 'dict', '10', '0', '0', '', '', 'validation_type', '4');
INSERT INTO `dev_field` VALUES ('19', '4', 'rule', '验证规则', 'varchar', 'string', '200', '0', '1', null, null, null, '5');
INSERT INTO `dev_field` VALUES ('20', '1', 'code', '代码', 'varchar', 'string', '20', '0', '0', '', null, '', '1');
INSERT INTO `dev_field` VALUES ('21', '1', 'title', '名称', 'varchar', 'string', '50', '0', '0', '', null, '', '2');
INSERT INTO `dev_field` VALUES ('22', '1', 'status', '状态', 'varchar', 'dict', '1', '0', '1', '1', null, 'status', '3');
INSERT INTO `dev_field` VALUES ('23', '2', 'type', '类型', 'varchar', 'string', '1', '0', '0', 'c', null, 'model_type', '3');
INSERT INTO `dev_field` VALUES ('24', '2', 'code', '代码', 'varchar', 'string', '20', '0', '0', '', null, '', '4');
INSERT INTO `dev_field` VALUES ('25', '2', 'title', '名称', 'varchar', 'string', '50', '0', '0', '', null, '', '5');
INSERT INTO `dev_field` VALUES ('26', '2', 'status', '状态', 'varchar', 'dict', '1', '0', '1', '1', null, 'status', '6');
INSERT INTO `dev_field` VALUES ('27', '5', 'title', '标题', 'varchar', 'string', '100', '0', '0', '', null, '', '1');
INSERT INTO `dev_field` VALUES ('28', '5', 'content', '内容', 'text', 'text', '200', '0', '0', '', null, '', '2');
INSERT INTO `dev_field` VALUES ('29', '5', 'add_time', '发布时间', 'datetime', 'datetime', '20', '0', '0', '', null, '', '3');
INSERT INTO `dev_field` VALUES ('30', '5', 'add_user_id', '发布人', 'number', 'number', '11', '0', '0', '', null, '', '4');
INSERT INTO `dev_field` VALUES ('31', '5', 'read_count', '阅读量', 'number', 'number', '11', '0', '0', '0', null, '', '5');
INSERT INTO `dev_field` VALUES ('35', '7', 'title', '应用名称', 'varchar', 'string', '50', '0', '0', null, null, null, '1');
INSERT INTO `dev_field` VALUES ('36', '7', 'type', '应用类型', 'varchar', 'dict', '50', '0', '0', null, null, 'app_type', '2');
INSERT INTO `dev_field` VALUES ('37', '7', 'url', '应用地址', 'varchar', 'string', '1', '0', '1', null, null, null, '3');
INSERT INTO `dev_field` VALUES ('38', '7', 'company', '厂商', 'varchar', 'string', '50', '0', '1', null, null, null, '4');
INSERT INTO `dev_field` VALUES ('39', '7', 'add_time', '接入时间', 'date', 'datetime', '10', '0', '1', '', null, '', '5');
INSERT INTO `dev_field` VALUES ('40', '7', 'status', '状态', 'varchar', 'dict', '1', '0', '1', '1', null, 'status', '6');
INSERT INTO `dev_field` VALUES ('41', '7', 'comment', '应用说明', 'text', 'text', '500', '0', '1', '', null, '', '7');
INSERT INTO `dev_field` VALUES ('42', '8', 'title', '数据流名称', 'varchar', 'string', '50', '0', '0', null, null, null, '1');
INSERT INTO `dev_field` VALUES ('43', '8', 'mode', '操作类型', 'varchar', 'dict', '2', '0', '0', null, null, 'pipe_mode', '2');
INSERT INTO `dev_field` VALUES ('49', '8', 'model_id', '数据模型', 'varchar', 'relation', '11', '0', '0', null, null, 'dev_models', '8');
INSERT INTO `dev_field` VALUES ('50', '8', 'sqls', 'SQL', 'text', 'text', '2000', '0', '1', null, null, null, '9');
INSERT INTO `dev_field` VALUES ('51', '8', 'add_time', '接入时间', 'date', 'datetime', '20', '0', '1', '', null, '', '10');
INSERT INTO `dev_field` VALUES ('52', '8', 'comment', '应用说明', 'text', 'text', '20', '0', '1', '', null, '', '11');
INSERT INTO `dev_field` VALUES ('55', '9', 'start_time', '开始时间', 'datetime', 'datetime', '11', '0', '0', '', null, '', '3');
INSERT INTO `dev_field` VALUES ('56', '9', 'end_time', '结束时间', 'datetime', 'datetime', '11', '0', '1', '', null, '', '4');
INSERT INTO `dev_field` VALUES ('57', '9', 'rows', '有效行数', 'number', 'number', '3', '0', '0', null, null, null, '5');
INSERT INTO `dev_field` VALUES ('58', '9', 'results', '是否成功', 'varchar', 'dict', '1', '0', '0', null, null, 'boolean', '6');
INSERT INTO `dev_field` VALUES ('59', '10', 'title', '应用名称', 'varchar', 'string', '50', '0', '0', '', null, '', '1');
INSERT INTO `dev_field` VALUES ('60', '10', 'url', '应用地址', 'varchar', 'string', '20', '0', '1', '', null, '', '2');
INSERT INTO `dev_field` VALUES ('61', '10', 'type', '应用类型', 'varchar', 'string', '1', '0', '0', 'i', null, 'app_type', '3');
INSERT INTO `dev_field` VALUES ('62', '10', 'add_time', '创建时间', 'datetime', 'datetime', '20', '0', '1', '', null, '', '4');
INSERT INTO `dev_field` VALUES ('63', '10', 'comment', '应用说明', 'text', 'text', '11', '0', '1', '', null, '', '5');
INSERT INTO `dev_field` VALUES ('64', '10', 'list_sort', '排序', 'number', 'number', '11', '0', '0', '9', null, '', '6');
INSERT INTO `dev_field` VALUES ('65', '10', 'icon', '图标', 'varchar', 'string', '20', '0', '1', '', null, '', '7');
INSERT INTO `dev_field` VALUES ('66', '10', 'status', '状态', 'varchar', 'dict', '1', '0', '1', '1', null, 'status', '8');
INSERT INTO `dev_field` VALUES ('67', '11', 'pid', '上级', 'number', 'number', '11', '0', '0', '0', null, '', '1');
INSERT INTO `dev_field` VALUES ('68', '11', 'type', '类型', 'varchar', 'string', '1', '0', '0', '', null, 'dict_type', '2');
INSERT INTO `dev_field` VALUES ('69', '11', 'code', '代码', 'varchar', 'string', '20', '0', '0', '', null, '', '3');
INSERT INTO `dev_field` VALUES ('70', '11', 'title', '名称', 'varchar', 'string', '50', '0', '0', '', null, '', '4');
INSERT INTO `dev_field` VALUES ('71', '11', 'list_sort', '排序', 'number', 'number', '11', '0', '0', '9', null, '', '5');
INSERT INTO `dev_field` VALUES ('72', '12', 'pid', '上级', 'number', 'number', '11', '0', '0', '0', null, '', '1');
INSERT INTO `dev_field` VALUES ('73', '12', 'type', '类型', 'varchar', 'string', '1', '0', '0', '', null, 'config_type', '2');
INSERT INTO `dev_field` VALUES ('74', '12', 'code', '代码', 'varchar', 'string', '20', '0', '0', '', null, '', '3');
INSERT INTO `dev_field` VALUES ('75', '12', 'title', '名称', 'varchar', 'string', '50', '0', '0', '', null, '', '4');
INSERT INTO `dev_field` VALUES ('76', '12', 'defaults', '默认值', 'varchar', 'string', '200', '0', '0', '', null, '', '5');
INSERT INTO `dev_field` VALUES ('77', '12', 'vals', '设置值', 'varchar', 'string', '200', '0', '1', '', null, '', '6');
INSERT INTO `dev_field` VALUES ('78', '12', 'list_sort', '排序', 'number', 'number', '11', '0', '0', '9', null, '', '7');
INSERT INTO `dev_field` VALUES ('79', '13', 'app_id', '所属应用', 'number', 'number', '11', '0', '0', '0', null, '', '1');
INSERT INTO `dev_field` VALUES ('80', '13', 'pid', '上级菜单', 'number', 'number', '11', '0', '0', '0', null, '', '2');
INSERT INTO `dev_field` VALUES ('81', '13', 'type', '菜单类型', 'varchar', 'string', '1', '0', '0', '', null, 'menu_type', '3');
INSERT INTO `dev_field` VALUES ('82', '13', 'url', '操作地址', 'varchar', 'string', '200', '0', '0', '', null, '', '4');
INSERT INTO `dev_field` VALUES ('83', '13', 'title', '菜单名称', 'varchar', 'string', '50', '0', '0', '', null, '', '5');
INSERT INTO `dev_field` VALUES ('84', '13', 'list_sort', '排序', 'number', 'number', '11', '0', '0', '9', null, '', '6');
INSERT INTO `dev_field` VALUES ('85', '13', 'icon', '图标', 'varchar', 'string', '20', '0', '1', '', null, '', '7');
INSERT INTO `dev_field` VALUES ('86', '13', 'status', '状态', 'varchar', 'dict', '1', '0', '1', '1', null, 'status', '8');
INSERT INTO `dev_field` VALUES ('87', '14', 'account', '账号', 'varchar', 'string', '50', '0', '0', '0', null, null, '1');
INSERT INTO `dev_field` VALUES ('88', '14', 'password', '密码', 'varchar', 'string', '32', '0', '1', null, null, null, '2');
INSERT INTO `dev_field` VALUES ('89', '14', 'email', '邮箱', 'varchar', 'string', '50', '0', '0', null, null, null, '3');
INSERT INTO `dev_field` VALUES ('90', '14', 'phone', '手机号', 'varchar', 'string', '20', '0', '0', null, null, null, '4');
INSERT INTO `dev_field` VALUES ('91', '14', 'nickname', '姓名', 'varchar', 'string', '100', '0', '0', null, null, null, '5');
INSERT INTO `dev_field` VALUES ('92', '14', 'create_time', '创建时间', 'datetime', 'datetime', '20', '0', '1', '', null, '', '6');
INSERT INTO `dev_field` VALUES ('93', '14', 'depart_id', '所在部门', 'varchar', 'relation', '11', '0', '1', null, null, 'sys_department', '7');
INSERT INTO `dev_field` VALUES ('94', '14', 'salt', '盐值', 'varchar', 'string', '6', '0', '1', null, null, null, '8');
INSERT INTO `dev_field` VALUES ('95', '14', 'status', '状态', 'varchar', 'dict', '1', '0', '0', '1', null, 'status', '9');
INSERT INTO `dev_field` VALUES ('96', '15', 'code', '代码', 'varchar', 'string', '20', '0', '0', '', null, '', '1');
INSERT INTO `dev_field` VALUES ('97', '15', 'title', '名称', 'varchar', 'string', '50', '0', '0', '', null, '', '2');
INSERT INTO `dev_field` VALUES ('98', '15', 'is_default', '是否默认', 'varchar', 'dict', '1', '0', '1', '0', null, 'boolean', '3');
INSERT INTO `dev_field` VALUES ('99', '15', 'status', '状态', 'varchar', 'dict', '1', '0', '1', '1', null, 'status', '4');
INSERT INTO `dev_field` VALUES ('104', '18', 'account', '账号', 'varchar', 'string', '50', '0', '0', '', null, '', '1');
INSERT INTO `dev_field` VALUES ('105', '18', 'user_id', '用户', 'number', 'number', '11', '0', '1', '0', null, '', '2');
INSERT INTO `dev_field` VALUES ('106', '18', 'ip', 'IP地址', 'varchar', 'string', '50', '0', '1', '', null, '', '3');
INSERT INTO `dev_field` VALUES ('107', '18', 'session_id', 'Session', 'varchar', 'string', '50', '0', '1', '', null, '', '4');
INSERT INTO `dev_field` VALUES ('108', '18', 'login_time', '登录时间', 'datetime', 'datetime', '20', '0', '1', '', null, '', '5');
INSERT INTO `dev_field` VALUES ('109', '18', 'logout_time', '退出时间', 'datetime', 'datetime', '20', '0', '1', '', null, '', '6');
INSERT INTO `dev_field` VALUES ('110', '19', 'code', '代码', 'varchar', 'string', '50', '0', '0', '', null, '', '1');
INSERT INTO `dev_field` VALUES ('111', '19', 'user_id', '用户', 'number', 'number', '11', '0', '1', '0', null, '', '2');
INSERT INTO `dev_field` VALUES ('112', '19', 'app_id', '应用', 'number', 'number', '11', '0', '1', '0', null, '', '3');
INSERT INTO `dev_field` VALUES ('113', '19', 'ip', 'IP地址', 'varchar', 'string', '50', '0', '1', '', null, '', '4');
INSERT INTO `dev_field` VALUES ('114', '19', 'create_time', '创建时间', 'datetime', 'datetime', '20', '0', '1', '', null, '', '5');
INSERT INTO `dev_field` VALUES ('115', '19', 'expire_time', '过期时间', 'datetime', 'datetime', '20', '0', '1', '', null, '', '6');
INSERT INTO `dev_field` VALUES ('116', '20', 'pid', '上级', 'number', 'number', '11', '0', '0', '0', null, '', '1');
INSERT INTO `dev_field` VALUES ('117', '20', 'code', '代码', 'varchar', 'string', '20', '0', '0', '', null, '', '3');
INSERT INTO `dev_field` VALUES ('118', '20', 'title', '名称', 'varchar', 'string', '50', '0', '0', '', null, '', '4');
INSERT INTO `dev_field` VALUES ('119', '20', 'list_sort', '排序', 'number', 'number', '11', '0', '0', '9', null, '', '5');
INSERT INTO `dev_field` VALUES ('120', '20', 'status', '状态', 'varchar', 'dict', '1', '0', '0', '1', null, 'status', '6');
INSERT INTO `dev_field` VALUES ('121', '21', 'name', '文件名', 'varchar', 'string', '100', '0', '0', '', null, '', '1');
INSERT INTO `dev_field` VALUES ('122', '21', 'exts', '扩展名', 'varchar', 'string', '10', '0', '0', '', null, '', '2');
INSERT INTO `dev_field` VALUES ('123', '21', 'md5', '唯一标识', 'varchar', 'string', '50', '0', '0', '', null, '', '3');
INSERT INTO `dev_field` VALUES ('124', '21', 'path', '保存路径', 'varchar', 'string', '200', '0', '0', '', null, '', '4');
INSERT INTO `dev_field` VALUES ('125', '21', 'upload_user_id', '上传用户', 'number', 'number', '11', '0', '0', '', null, '', '5');
INSERT INTO `dev_field` VALUES ('126', '21', 'upload_time', '上传时间', 'datetime', 'datetime', '20', '0', '1', '', null, '', '6');
INSERT INTO `dev_field` VALUES ('127', '21', 'size', '文件大小', 'number', 'number', '11', '0', '1', '', null, '', '7');
INSERT INTO `dev_field` VALUES ('128', '21', 'cover', '封面', 'varchar', 'string', '200', '0', '1', '', null, '', '8');
INSERT INTO `dev_field` VALUES ('129', '21', 'length', '长度', 'number', 'number', '11', '0', '1', '', null, '', '9');
INSERT INTO `dev_field` VALUES ('130', '21', 'pre_file', '预览文件', 'varchar', 'file', '200', '0', '1', '', null, '', '10');
INSERT INTO `dev_field` VALUES ('131', '21', 'status', '状态', 'varchar', 'dict', '1', '0', '0', '1', null, 'status', '11');
INSERT INTO `dev_field` VALUES ('132', '22', 'user_id', '操作用户', 'number', 'number', '11', '0', '0', '0', null, '', '1');
INSERT INTO `dev_field` VALUES ('133', '22', 'action_id', '操作动作', 'number', 'number', '11', '0', '0', '0', null, '', '2');
INSERT INTO `dev_field` VALUES ('134', '22', 'action_time', '上传时间', 'datetime', 'datetime', '20', '0', '0', 'now', null, null, '3');
INSERT INTO `dev_field` VALUES ('135', '22', 'param', '参数', 'varchar', 'string', '200', '0', '0', null, null, null, '4');
INSERT INTO `dev_field` VALUES ('136', '22', 'ip', '操作IP', 'varchar', 'string', '50', '0', '0', '9', null, null, '5');
INSERT INTO `dev_field` VALUES ('137', '23', 'url', '操作地址', 'varchar', 'string', '50', '0', '0', null, null, null, '1');
INSERT INTO `dev_field` VALUES ('138', '23', 'title', '名称', 'varchar', 'string', '50', '0', '0', null, null, null, '2');
INSERT INTO `dev_field` VALUES ('139', '23', 'module_id', '所属模块', 'varchar', 'relation', '11', '0', '0', null, null, 'dev_module', '3');
INSERT INTO `dev_field` VALUES ('140', '23', 'model_id', '所属模型', 'varchar', 'relation', '11', '0', '0', null, null, 'dev_models', '4');
INSERT INTO `dev_field` VALUES ('141', '24', 'pid', '上级部门', 'number', 'relation', '11', '0', '0', '0', null, 'sys_department', '1');
INSERT INTO `dev_field` VALUES ('142', '24', 'type', '部门类型', 'varchar', 'dict', '1', '0', '0', null, null, 'depart_type', '2');
INSERT INTO `dev_field` VALUES ('143', '24', 'code', '部门代码', 'varchar', 'string', '50', '0', '0', null, null, null, '3');
INSERT INTO `dev_field` VALUES ('144', '24', 'title', '部门名称', 'varchar', 'string', '100', '0', '0', null, null, null, '4');
INSERT INTO `dev_field` VALUES ('145', '24', 'list_sort', '排序', 'number', 'number', '11', '0', '0', '9', null, null, '5');
INSERT INTO `dev_field` VALUES ('146', '24', 'status', '状态', 'varchar', 'dict', '1', '0', '1', '1', null, 'status', '6');
INSERT INTO `dev_field` VALUES ('147', '25', 'cate_id', '分类', 'varchar', 'number', '11', '0', '1', '0', '', '', '1');
INSERT INTO `dev_field` VALUES ('148', '25', 'code', '数据集代码', 'varchar', 'string', '50', '0', '0', '', '', '', '2');
INSERT INTO `dev_field` VALUES ('149', '25', 'title', '数据集名称', 'varchar', 'string', '100', '0', '0', '', '', '', '3');
INSERT INTO `dev_field` VALUES ('150', '25', 'sqls', '查询SQL', 'text', 'text', '500', '0', '0', '', null, '', '4');
INSERT INTO `dev_field` VALUES ('151', '25', 'list_sort', '排序', 'varchar', 'number', '11', '0', '1', '9', '', '', '5');
INSERT INTO `dev_field` VALUES ('152', '25', 'comment', '说明', 'varchar', 'text', '200', '0', '1', '', '', '', '6');
INSERT INTO `dev_field` VALUES ('153', '25', 'status', '状态', 'varchar', 'dict', '1', '0', '1', '1', '', 'status', '7');
INSERT INTO `dev_field` VALUES ('154', '26', 'cate_id', '分类', 'varchar', 'relation', '11', '0', '0', '0', '', 'sys_category', '1');
INSERT INTO `dev_field` VALUES ('155', '26', 'code', '代码', 'varchar', 'string', '50', '0', '0', '', '', '', '2');
INSERT INTO `dev_field` VALUES ('156', '26', 'title', '名称', 'varchar', 'string', '100', '0', '0', '', '', '', '3');
INSERT INTO `dev_field` VALUES ('157', '26', 'ds_id', '数据源', 'varchar', 'relation', '11', '0', '0', '', '', 'dev_dataset', '4');
INSERT INTO `dev_field` VALUES ('158', '26', 'type', '类型', 'varchar', 'dict', '20', '0', '0', 'bar', '', 'chart_type', '5');
INSERT INTO `dev_field` VALUES ('160', '26', 'comment', '说明', 'varchar', 'text', '200', '0', '1', '', '', '', '7');
INSERT INTO `dev_field` VALUES ('161', '26', 'list_sort', '排序', 'varchar', 'number', '11', '0', '0', '9', '', '', '8');
INSERT INTO `dev_field` VALUES ('162', '26', 'status', '状态', 'varchar', 'dict', '1', '0', '0', '1', '', 'status', '9');
INSERT INTO `dev_field` VALUES ('163', '8', 'status', '状态', 'varchar', 'dict', '1', '0', '0', '1', null, 'status', '9');
INSERT INTO `dev_field` VALUES ('164', '7', 'db_type', '数据库类型', 'varchar', 'string', '20', '0', '1', null, null, null, '9');
INSERT INTO `dev_field` VALUES ('165', '7', 'driver', 'JDBC驱动', 'varchar', 'string', '50', '0', '1', null, null, null, '9');
INSERT INTO `dev_field` VALUES ('166', '7', 'jdbc', 'JDBC链接', 'varchar', 'string', '200', '0', '1', null, null, null, '9');
INSERT INTO `dev_field` VALUES ('167', '7', 'db_user', '数据库账号', 'varchar', 'string', '50', '0', '1', null, null, null, '9');
INSERT INTO `dev_field` VALUES ('168', '7', 'db_pass', '数据库密码', 'varchar', 'string', '50', '0', '1', null, null, null, '9');
INSERT INTO `dev_field` VALUES ('169', '8', 'client_id', '接入应用', 'number', 'number', '10', '0', '0', null, null, null, '9');
INSERT INTO `dev_field` VALUES ('170', '8', 'list_sort', '优先级', 'number', 'number', '10', '0', '0', '9', null, null, '9');
INSERT INTO `dev_field` VALUES ('171', '9', 'client_id', '接入应用', 'number', 'relation', '10', '0', '0', null, null, 'dev_client', '9');
INSERT INTO `dev_field` VALUES ('172', '9', 'model_id', '数据模型', 'number', 'relation', '10', '0', '0', null, null, 'dev_models', '9');
INSERT INTO `dev_field` VALUES ('173', '9', 'mode', '操作类型', 'varchar', 'dict', '2', '0', '0', null, null, 'pipe_mode', '9');
INSERT INTO `dev_field` VALUES ('174', '27', 'code', '学号', 'varchar', 'string', '50', '0', '0', null, null, null, '1');
INSERT INTO `dev_field` VALUES ('175', '27', 'name', '姓名', 'varchar', 'string', '200', '0', '0', null, null, null, '2');
INSERT INTO `dev_field` VALUES ('176', '27', 'phone', '手机号', 'varchar', 'string', '20', '0', '0', null, null, null, '3');
INSERT INTO `dev_field` VALUES ('177', '27', 'birthday', '出生日期', 'date', 'date', '10', '0', '1', null, null, null, '9');
INSERT INTO `dev_field` VALUES ('178', '7', 'code', '代码', 'varchar', 'string', '32', '0', '1', 'random:16', null, null, '9');
INSERT INTO `dev_field` VALUES ('179', '7', 'secret', '密钥', 'varchar', 'string', '32', '0', '1', 'random:16', null, null, '9');

-- ----------------------------
-- Table structure for dev_models
-- ----------------------------
DROP TABLE IF EXISTS `dev_models`;
CREATE TABLE `dev_models` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `module_id` int(11) NOT NULL COMMENT '所属模块',
  `tables` varchar(100) NOT NULL COMMENT '数据表',
  `type` char(1) NOT NULL COMMENT '类型',
  `code` varchar(20) NOT NULL COMMENT '代码',
  `title` varchar(50) NOT NULL COMMENT '名称',
  `pk_field` varchar(100) DEFAULT NULL COMMENT '主键字段',
  `status` char(1) DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COMMENT='模型';

-- ----------------------------
-- Records of dev_models
-- ----------------------------
INSERT INTO `dev_models` VALUES ('1', '1', 'dev_module', 'c', 'module', '模块', null, '1');
INSERT INTO `dev_models` VALUES ('2', '1', 'dev_models', 'c', 'models', '模型', null, '1');
INSERT INTO `dev_models` VALUES ('3', '1', 'dev_field', 'c', 'field', '字段', null, '1');
INSERT INTO `dev_models` VALUES ('4', '1', 'dev_validation', 'c', 'validation', '验证', null, '1');
INSERT INTO `dev_models` VALUES ('5', '2', 'sys_notice', 'c', 'notice', '公告', null, '1');
INSERT INTO `dev_models` VALUES ('7', '1', 'dev_client', 'c', 'client', '客户端', null, '1');
INSERT INTO `dev_models` VALUES ('8', '1', 'dev_pipe', 'c', 'pipe', '数据流', null, '1');
INSERT INTO `dev_models` VALUES ('9', '1', 'dev_logs', 'c', 'logs', '日志', null, '1');
INSERT INTO `dev_models` VALUES ('10', '2', 'sys_application', 'c', 'application', '应用', null, '1');
INSERT INTO `dev_models` VALUES ('11', '2', 'sys_dict', 't', 'dict', '字典', null, '1');
INSERT INTO `dev_models` VALUES ('12', '2', 'sys_config', 't', 'config', '配置', null, '1');
INSERT INTO `dev_models` VALUES ('13', '2', 'sys_menu', 't', 'menu', '菜单', null, '1');
INSERT INTO `dev_models` VALUES ('14', '2', 'sys_user', 'c', 'user', '用户', null, '1');
INSERT INTO `dev_models` VALUES ('15', '2', 'sys_role', 'c', 'role', '角色', null, '1');
INSERT INTO `dev_models` VALUES ('18', '2', 'sys_user_login', 'c', 'userLogin', '登录日志', null, '1');
INSERT INTO `dev_models` VALUES ('19', '2', 'sys_token', 'c', 'token', '令牌', null, '1');
INSERT INTO `dev_models` VALUES ('20', '2', 'sys_category', 't', 'category', '分类', null, '1');
INSERT INTO `dev_models` VALUES ('21', '2', 'sys_file', 'c', 'file', '文件', null, '1');
INSERT INTO `dev_models` VALUES ('22', '2', 'sys_log', 'c', 'log', '日志', null, '1');
INSERT INTO `dev_models` VALUES ('23', '2', 'sys_action', 'c', 'action', '动作', null, '1');
INSERT INTO `dev_models` VALUES ('24', '2', 'sys_department', 't', 'department', '组织结构', null, '1');
INSERT INTO `dev_models` VALUES ('25', '1', 'dev_dataset', 'c', 'dataset', '数据集', null, '1');
INSERT INTO `dev_models` VALUES ('26', '1', 'dev_chart', 'c', 'chart', '报表', null, '1');
INSERT INTO `dev_models` VALUES ('27', '3', 'xdata_student', 'd', 'student', '学生', 'code', '1');

-- ----------------------------
-- Table structure for dev_module
-- ----------------------------
DROP TABLE IF EXISTS `dev_module`;
CREATE TABLE `dev_module` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `code` varchar(20) NOT NULL COMMENT '代码',
  `title` varchar(50) NOT NULL COMMENT '名称',
  `status` char(1) DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dev_module
-- ----------------------------
INSERT INTO `dev_module` VALUES ('1', 'dev', '开发', '1');
INSERT INTO `dev_module` VALUES ('2', 'sys', '核心', '1');
INSERT INTO `dev_module` VALUES ('3', 'xdata', '数据中心', '1');

-- ----------------------------
-- Table structure for dev_validation
-- ----------------------------
DROP TABLE IF EXISTS `dev_validation`;
CREATE TABLE `dev_validation` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `title` varchar(50) NOT NULL COMMENT '规则名称',
  `model_id` int(11) NOT NULL COMMENT '所属模型',
  `field_id` int(11) NOT NULL COMMENT '字段',
  `type` varchar(10) NOT NULL COMMENT '验证类型',
  `rule` varchar(200) DEFAULT NULL COMMENT '验证规则',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=111 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dev_validation
-- ----------------------------
INSERT INTO `dev_validation` VALUES ('1', '代码长度不能大于20', '3', '4', 'length', '20');
INSERT INTO `dev_validation` VALUES ('2', '名称长度不能大于50', '3', '5', 'length', '50');
INSERT INTO `dev_validation` VALUES ('3', '数据库类型长度不能大于10', '3', '6', 'length', '10');
INSERT INTO `dev_validation` VALUES ('4', '业务类型长度不能大于10', '3', '7', 'length', '10');
INSERT INTO `dev_validation` VALUES ('5', '空值长度不能大于1', '3', '10', 'length', '1');
INSERT INTO `dev_validation` VALUES ('6', '默认值长度不能大于100', '3', '11', 'length', '100');
INSERT INTO `dev_validation` VALUES ('7', '自动计算公式长度不能大于200', '3', '12', 'length', '200');
INSERT INTO `dev_validation` VALUES ('8', '参数长度不能大于200', '3', '13', 'length', '200');
INSERT INTO `dev_validation` VALUES ('9', '规则名称长度不能大于50', '4', '15', 'length', '50');
INSERT INTO `dev_validation` VALUES ('10', '验证类型长度不能大于10', '4', '18', 'length', '10');
INSERT INTO `dev_validation` VALUES ('11', '验证规则长度不能大于200', '4', '19', 'length', '200');
INSERT INTO `dev_validation` VALUES ('12', '代码长度不能大于20', '1', '20', 'length', '20');
INSERT INTO `dev_validation` VALUES ('13', '名称长度不能大于50', '1', '21', 'length', '50');
INSERT INTO `dev_validation` VALUES ('14', '数据表长度不能大于100', '2', '2', 'length', '100');
INSERT INTO `dev_validation` VALUES ('15', '类型长度不能大于1', '2', '23', 'length', '1');
INSERT INTO `dev_validation` VALUES ('16', '代码长度不能大于20', '2', '24', 'length', '20');
INSERT INTO `dev_validation` VALUES ('17', '名称长度不能大于50', '2', '25', 'length', '50');
INSERT INTO `dev_validation` VALUES ('18', '标题长度不能大于100', '5', '27', 'length', '100');
INSERT INTO `dev_validation` VALUES ('19', '发布时间必须是日期时间格式:2019-03-06 12:24:00', '5', '29', 'regex', 'datetime');
INSERT INTO `dev_validation` VALUES ('20', '阅读量必须是数字格式', '5', '31', 'regex', 'number');
INSERT INTO `dev_validation` VALUES ('22', '应用名称长度不能大于50', '7', '35', 'length', '50');
INSERT INTO `dev_validation` VALUES ('23', '应用类型长度不能大于50', '7', '36', 'length', '50');
INSERT INTO `dev_validation` VALUES ('24', '应用地址必须是URL地址格式:http://xxx', '7', '37', 'regex', 'url');
INSERT INTO `dev_validation` VALUES ('25', '应用地址长度不能大于100', '7', '37', 'length', '100');
INSERT INTO `dev_validation` VALUES ('26', '厂商长度不能大于50', '7', '38', 'length', '50');
INSERT INTO `dev_validation` VALUES ('27', '接入时间必须是日期时间格式:2019-03-06 12:24:00', '7', '39', 'regex', 'datetime');
INSERT INTO `dev_validation` VALUES ('28', '数据流名称长度不能大于50', '8', '42', 'length', '50');
INSERT INTO `dev_validation` VALUES ('29', '操作类型长度不能大于2', '8', '43', 'length', '2');
INSERT INTO `dev_validation` VALUES ('36', '接入时间必须是日期时间格式:2019-03-06 12:24:00', '8', '51', 'regex', 'datetime');
INSERT INTO `dev_validation` VALUES ('38', '开始时间必须是日期时间格式:2019-03-06 12:24:00', '9', '55', 'regex', 'datetime');
INSERT INTO `dev_validation` VALUES ('39', '结束时间必须是日期时间格式:2019-03-06 12:24:00', '9', '56', 'regex', 'datetime');
INSERT INTO `dev_validation` VALUES ('40', '是否成功长度不能大于1', '9', '58', 'length', '1');
INSERT INTO `dev_validation` VALUES ('41', '应用名称长度不能大于50', '10', '59', 'length', '50');
INSERT INTO `dev_validation` VALUES ('42', '应用地址必须是URL地址格式:http://xxx', '10', '60', 'regex', 'url');
INSERT INTO `dev_validation` VALUES ('43', '应用地址长度不能大于20', '10', '60', 'length', '20');
INSERT INTO `dev_validation` VALUES ('44', '应用类型长度不能大于1', '10', '61', 'length', '1');
INSERT INTO `dev_validation` VALUES ('45', '创建时间必须是日期时间格式:2019-03-06 12:24:00', '10', '62', 'regex', 'datetime');
INSERT INTO `dev_validation` VALUES ('46', '图标长度不能大于20', '10', '65', 'length', '20');
INSERT INTO `dev_validation` VALUES ('47', '类型长度不能大于1', '11', '68', 'length', '1');
INSERT INTO `dev_validation` VALUES ('48', '代码长度不能大于20', '11', '69', 'length', '20');
INSERT INTO `dev_validation` VALUES ('49', '名称长度不能大于50', '11', '70', 'length', '50');
INSERT INTO `dev_validation` VALUES ('50', '类型长度不能大于1', '12', '73', 'length', '1');
INSERT INTO `dev_validation` VALUES ('51', '代码长度不能大于20', '12', '74', 'length', '20');
INSERT INTO `dev_validation` VALUES ('52', '名称长度不能大于50', '12', '75', 'length', '50');
INSERT INTO `dev_validation` VALUES ('53', '默认值长度不能大于200', '12', '76', 'length', '200');
INSERT INTO `dev_validation` VALUES ('54', '设置值长度不能大于200', '12', '77', 'length', '200');
INSERT INTO `dev_validation` VALUES ('55', '菜单类型长度不能大于1', '13', '81', 'length', '1');
INSERT INTO `dev_validation` VALUES ('57', '操作地址长度不能大于200', '13', '82', 'length', '200');
INSERT INTO `dev_validation` VALUES ('58', '菜单名称长度不能大于50', '13', '83', 'length', '50');
INSERT INTO `dev_validation` VALUES ('59', '图标长度不能大于20', '13', '85', 'length', '20');
INSERT INTO `dev_validation` VALUES ('60', '账号长度不能大于50', '14', '87', 'length', '50');
INSERT INTO `dev_validation` VALUES ('61', '密码长度不能大于32', '14', '88', 'length', '32');
INSERT INTO `dev_validation` VALUES ('62', '邮箱必须是邮箱格式:XXX@XX.com', '14', '89', 'regex', 'email');
INSERT INTO `dev_validation` VALUES ('63', '手机号必须是手机号码格式:1xxxxxxxxxx', '14', '90', 'regex', 'phone');
INSERT INTO `dev_validation` VALUES ('64', '手机号长度不能大于20', '14', '90', 'length', '20');
INSERT INTO `dev_validation` VALUES ('65', '姓名长度不能大于100', '14', '91', 'length', '100');
INSERT INTO `dev_validation` VALUES ('66', '创建时间必须是日期时间格式:2019-03-06 12:24:00', '14', '92', 'regex', 'datetime');
INSERT INTO `dev_validation` VALUES ('67', '盐值长度不能大于6', '14', '94', 'length', '6');
INSERT INTO `dev_validation` VALUES ('68', '代码长度不能大于20', '15', '96', 'length', '20');
INSERT INTO `dev_validation` VALUES ('69', '名称长度不能大于50', '15', '97', 'length', '50');
INSERT INTO `dev_validation` VALUES ('70', '账号长度不能大于50', '18', '104', 'length', '50');
INSERT INTO `dev_validation` VALUES ('71', 'IP地址长度不能大于50', '18', '106', 'length', '50');
INSERT INTO `dev_validation` VALUES ('72', 'Session长度不能大于50', '18', '107', 'length', '50');
INSERT INTO `dev_validation` VALUES ('73', '登录时间必须是日期时间格式:2019-03-06 12:24:00', '18', '108', 'regex', 'datetime');
INSERT INTO `dev_validation` VALUES ('74', '退出时间必须是日期时间格式:2019-03-06 12:24:00', '18', '109', 'regex', 'datetime');
INSERT INTO `dev_validation` VALUES ('75', '代码长度不能大于50', '19', '110', 'length', '50');
INSERT INTO `dev_validation` VALUES ('76', 'IP地址长度不能大于50', '19', '113', 'length', '50');
INSERT INTO `dev_validation` VALUES ('77', '创建时间必须是日期时间格式:2019-03-06 12:24:00', '19', '114', 'regex', 'datetime');
INSERT INTO `dev_validation` VALUES ('78', '过期时间必须是日期时间格式:2019-03-06 12:24:00', '19', '115', 'regex', 'datetime');
INSERT INTO `dev_validation` VALUES ('79', '代码长度不能大于20', '20', '117', 'length', '20');
INSERT INTO `dev_validation` VALUES ('80', '名称长度不能大于50', '20', '118', 'length', '50');
INSERT INTO `dev_validation` VALUES ('81', '文件名长度不能大于100', '21', '121', 'length', '100');
INSERT INTO `dev_validation` VALUES ('82', '扩展名长度不能大于10', '21', '122', 'length', '10');
INSERT INTO `dev_validation` VALUES ('83', '唯一标识长度不能大于50', '21', '123', 'length', '50');
INSERT INTO `dev_validation` VALUES ('84', '保存路径长度不能大于200', '21', '124', 'length', '200');
INSERT INTO `dev_validation` VALUES ('85', '上传时间必须是日期时间格式:2019-03-06 12:24:00', '21', '126', 'regex', 'datetime');
INSERT INTO `dev_validation` VALUES ('86', '封面长度不能大于200', '21', '128', 'length', '200');
INSERT INTO `dev_validation` VALUES ('87', '上传时间必须是日期时间格式:2019-03-06 12:24:00', '22', '134', 'regex', 'datetime');
INSERT INTO `dev_validation` VALUES ('88', '参数长度不能大于200', '22', '135', 'length', '200');
INSERT INTO `dev_validation` VALUES ('89', '操作IP长度不能大于50', '22', '136', 'length', '50');
INSERT INTO `dev_validation` VALUES ('91', '操作地址长度不能大于50', '23', '137', 'length', '50');
INSERT INTO `dev_validation` VALUES ('92', '名称长度不能大于50', '23', '138', 'length', '50');
INSERT INTO `dev_validation` VALUES ('93', '部门类型长度不能大于1', '24', '142', 'length', '1');
INSERT INTO `dev_validation` VALUES ('94', '部门代码长度不能大于50', '24', '143', 'length', '50');
INSERT INTO `dev_validation` VALUES ('95', '部门名称长度不能大于100', '24', '144', 'length', '100');
INSERT INTO `dev_validation` VALUES ('96', '部门代码长度不能大于50', '25', '148', 'length', '50');
INSERT INTO `dev_validation` VALUES ('97', '部门名称长度不能大于100', '25', '149', 'length', '100');
INSERT INTO `dev_validation` VALUES ('98', '说明长度不能大于200', '25', '152', 'length', '200');
INSERT INTO `dev_validation` VALUES ('99', '代码长度不能大于50', '26', '155', 'length', '50');
INSERT INTO `dev_validation` VALUES ('100', '名称长度不能大于100', '26', '156', 'length', '100');
INSERT INTO `dev_validation` VALUES ('101', '类型长度不能大于20', '26', '158', 'length', '20');
INSERT INTO `dev_validation` VALUES ('102', '说明长度不能大于200', '26', '160', 'length', '200');
INSERT INTO `dev_validation` VALUES ('107', '报表代码必须唯一', '26', '155', 'unique', '0');
INSERT INTO `dev_validation` VALUES ('108', '手机号', '27', '176', 'regex', 'phone');
INSERT INTO `dev_validation` VALUES ('109', '客户端代码必须唯一', '7', '178', 'unique', null);
INSERT INTO `dev_validation` VALUES ('110', '学号必须唯一', '27', '174', 'unique', null);

-- ----------------------------
-- Table structure for sys_action
-- ----------------------------
DROP TABLE IF EXISTS `sys_action`;
CREATE TABLE `sys_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `url` varchar(50) NOT NULL COMMENT '操作地址',
  `title` varchar(50) NOT NULL COMMENT '名称',
  `module_id` int(11) NOT NULL COMMENT '所属模块',
  `model_id` int(11) NOT NULL COMMENT '所属模型',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_action
-- ----------------------------
INSERT INTO `sys_action` VALUES ('1', 'post:/login', '登录', '2', '14');

-- ----------------------------
-- Table structure for sys_application
-- ----------------------------
DROP TABLE IF EXISTS `sys_application`;
CREATE TABLE `sys_application` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `title` varchar(50) NOT NULL COMMENT '应用名称',
  `url` varchar(20) DEFAULT NULL COMMENT '应用地址',
  `type` char(1) NOT NULL COMMENT '应用类型',
  `add_time` datetime DEFAULT NULL COMMENT '创建时间',
  `comment` text COMMENT '应用说明',
  `list_sort` int(11) NOT NULL COMMENT '排序',
  `icon` varchar(20) DEFAULT NULL COMMENT '图标',
  `status` char(1) DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_application
-- ----------------------------
INSERT INTO `sys_application` VALUES ('1', '开发', 'dev', 'i', '2020-03-11 15:32:42', '', '9', '', '1');
INSERT INTO `sys_application` VALUES ('2', '核心', 'sys', 'i', '2020-03-11 15:32:42', '', '9', '', '1');

-- ----------------------------
-- Table structure for sys_category
-- ----------------------------
DROP TABLE IF EXISTS `sys_category`;
CREATE TABLE `sys_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `pid` int(11) NOT NULL COMMENT '上级',
  `code` varchar(20) NOT NULL COMMENT '代码',
  `title` varchar(50) NOT NULL COMMENT '名称',
  `list_sort` int(11) NOT NULL COMMENT '排序',
  `status` char(1) NOT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_category
-- ----------------------------
INSERT INTO `sys_category` VALUES ('1', '0', 'student', '学生', '1', '1');
INSERT INTO `sys_category` VALUES ('2', '0', 'teacher', '教师', '9', '1');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `pid` int(11) NOT NULL COMMENT '上级',
  `type` char(1) NOT NULL COMMENT '类型',
  `code` varchar(20) NOT NULL COMMENT '代码',
  `title` varchar(50) NOT NULL COMMENT '名称',
  `defaults` varchar(200) NOT NULL COMMENT '默认值',
  `vals` varchar(200) DEFAULT NULL COMMENT '设置值',
  `list_sort` int(11) NOT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('1', '0', 'g', 'core', '核心配置', '', null, '1');
INSERT INTO `sys_config` VALUES ('2', '0', 'g', 'sys', '系统配置', '', null, '92');
INSERT INTO `sys_config` VALUES ('3', '2', 't', 'title', '系统名称', '管理平台', '管理平台 ', '1');
INSERT INTO `sys_config` VALUES ('4', '1', 'n', 'file_size', '附件大小', '10240', '1024', '1');

-- ----------------------------
-- Table structure for sys_department
-- ----------------------------
DROP TABLE IF EXISTS `sys_department`;
CREATE TABLE `sys_department` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `pid` int(11) NOT NULL COMMENT '上级部门',
  `type` char(1) NOT NULL COMMENT '部门类型',
  `code` varchar(50) NOT NULL COMMENT '部门代码',
  `title` varchar(100) NOT NULL COMMENT '部门名称',
  `list_sort` int(11) NOT NULL COMMENT '排序',
  `status` char(1) DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_department
-- ----------------------------
INSERT INTO `sys_department` VALUES ('1', '0', '0', '10', '教务', '1', '1');
INSERT INTO `sys_department` VALUES ('2', '0', '0', '21', '财务', '2', '1');

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `pid` int(11) NOT NULL COMMENT '上级',
  `type` char(1) NOT NULL COMMENT '类型',
  `code` varchar(20) NOT NULL COMMENT '代码',
  `title` varchar(50) NOT NULL COMMENT '名称',
  `list_sort` int(11) NOT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('1', '0', 'd', 'model_type', '模型类型', '99');
INSERT INTO `sys_dict` VALUES ('2', '1', 'v', 'c', '通用', '1');
INSERT INTO `sys_dict` VALUES ('3', '1', 'v', 't', '树形', '2');
INSERT INTO `sys_dict` VALUES ('4', '1', 'v', 'd', '数据', '3');
INSERT INTO `sys_dict` VALUES ('7', '0', 'd', 'db_type', '数据类型', '99');
INSERT INTO `sys_dict` VALUES ('8', '7', 'v', 'varchar', '文本', '1');
INSERT INTO `sys_dict` VALUES ('9', '7', 'v', 'text', '长文本', '2');
INSERT INTO `sys_dict` VALUES ('10', '7', 'v', 'number', '数字', '3');
INSERT INTO `sys_dict` VALUES ('11', '7', 'v', 'date', '日期', '4');
INSERT INTO `sys_dict` VALUES ('12', '7', 'v', 'datetime', '日期时间', '5');
INSERT INTO `sys_dict` VALUES ('13', '0', 'd', 'field_type', '字段类型', '99');
INSERT INTO `sys_dict` VALUES ('14', '13', 'v', 'string', '文本', '1');
INSERT INTO `sys_dict` VALUES ('15', '13', 'v', 'text', '长文本', '2');
INSERT INTO `sys_dict` VALUES ('16', '13', 'v', 'number', '数字', '3');
INSERT INTO `sys_dict` VALUES ('17', '13', 'v', 'html', '富文本', '4');
INSERT INTO `sys_dict` VALUES ('18', '13', 'v', 'file', '文件', '5');
INSERT INTO `sys_dict` VALUES ('19', '13', 'v', 'dict', '字典', '6');
INSERT INTO `sys_dict` VALUES ('20', '13', 'v', 'date', '日期', '7');
INSERT INTO `sys_dict` VALUES ('21', '13', 'v', 'datetime', '日期时间', '8');
INSERT INTO `sys_dict` VALUES ('23', '0', 'd', 'dict_type', '字典类型', '99');
INSERT INTO `sys_dict` VALUES ('24', '23', 'v', 'c', '分类', '1');
INSERT INTO `sys_dict` VALUES ('25', '23', 'v', 'd', '字典', '2');
INSERT INTO `sys_dict` VALUES ('26', '23', 'v', 'v', '值', '3');
INSERT INTO `sys_dict` VALUES ('27', '0', 'd', 'config_type', '配置类型', '99');
INSERT INTO `sys_dict` VALUES ('28', '27', 'v', 'g', '分组', '1');
INSERT INTO `sys_dict` VALUES ('29', '27', 'v', 'n', '数字', '2');
INSERT INTO `sys_dict` VALUES ('30', '27', 'v', 't', '文本', '3');
INSERT INTO `sys_dict` VALUES ('31', '27', 'v', 'p', '图片', '4');
INSERT INTO `sys_dict` VALUES ('32', '27', 'v', 'd', '日期', '5');
INSERT INTO `sys_dict` VALUES ('33', '0', 'd', 'menu_type', '菜单类型', '99');
INSERT INTO `sys_dict` VALUES ('34', '33', 'v', '0', '分组', '1');
INSERT INTO `sys_dict` VALUES ('35', '33', 'v', '1', '内部功能', '2');
INSERT INTO `sys_dict` VALUES ('36', '33', 'v', '2', '外部链接', '3');
INSERT INTO `sys_dict` VALUES ('37', '0', 'd', 'course_type', '分类类型', '99');
INSERT INTO `sys_dict` VALUES ('38', '37', 'v', '1', '点播课程', '1');
INSERT INTO `sys_dict` VALUES ('39', '37', 'v', '2', '网络课堂', '2');
INSERT INTO `sys_dict` VALUES ('40', '0', 'd', 'app_type', '应用类型', '99');
INSERT INTO `sys_dict` VALUES ('41', '40', 'v', 'i', '内置模块', '1');
INSERT INTO `sys_dict` VALUES ('42', '40', 'v', 'd', '桌面应用', '2');
INSERT INTO `sys_dict` VALUES ('43', '40', 'v', 'm', '移动应用', '3');
INSERT INTO `sys_dict` VALUES ('44', '40', 'v', 'h', 'HTML5', '4');
INSERT INTO `sys_dict` VALUES ('45', '40', 'v', 'w', 'WEB应用', '5');
INSERT INTO `sys_dict` VALUES ('46', '0', 'd', 'depart_type', '部门类型', '99');
INSERT INTO `sys_dict` VALUES ('47', '46', 'v', '0', '普通部门', '1');
INSERT INTO `sys_dict` VALUES ('48', '0', 'd', 'chart_type', '图表类型', '99');
INSERT INTO `sys_dict` VALUES ('49', '48', 'v', 'table', '数据表格', '1');
INSERT INTO `sys_dict` VALUES ('50', '48', 'v', 'line', '折线图', '2');
INSERT INTO `sys_dict` VALUES ('51', '48', 'v', 'bar', '柱状图', '3');
INSERT INTO `sys_dict` VALUES ('52', '48', 'v', 'pie', '饼图', '4');
INSERT INTO `sys_dict` VALUES ('53', '48', 'v', 'gauge', '仪表盘', '5');
INSERT INTO `sys_dict` VALUES ('54', '0', 'd', 'msg_type', '消息类型', '99');
INSERT INTO `sys_dict` VALUES ('55', '54', 'v', '1', '点对点', '1');
INSERT INTO `sys_dict` VALUES ('56', '54', 'v', '2', '一对多', '2');
INSERT INTO `sys_dict` VALUES ('57', '0', 'd', 'pipe_mode', '数据流类型', '99');
INSERT INTO `sys_dict` VALUES ('58', '57', 'v', 'r', '只读', '1');
INSERT INTO `sys_dict` VALUES ('59', '57', 'v', 'w', '只写', '2');
INSERT INTO `sys_dict` VALUES ('61', '0', 'd', 'validation_type', '验证规则类型', '99');
INSERT INTO `sys_dict` VALUES ('62', '61', 'v', 'regex', '正则匹配', '1');
INSERT INTO `sys_dict` VALUES ('63', '61', 'v', 'length', '长度限制', '2');
INSERT INTO `sys_dict` VALUES ('64', '61', 'v', 'between', '数字范围', '3');
INSERT INTO `sys_dict` VALUES ('65', '61', 'v', 'in', '枚举', '4');
INSERT INTO `sys_dict` VALUES ('66', '61', 'v', 'sql', '数据库查询', '6');
INSERT INTO `sys_dict` VALUES ('67', '0', 'd', 'status', '状态', '99');
INSERT INTO `sys_dict` VALUES ('68', '67', 'v', '1', '启用', '1');
INSERT INTO `sys_dict` VALUES ('69', '67', 'v', '0', '禁用', '2');
INSERT INTO `sys_dict` VALUES ('70', '0', 'd', 'boolean', '是否', '9');
INSERT INTO `sys_dict` VALUES ('71', '70', 'v', '1', '是', '1');
INSERT INTO `sys_dict` VALUES ('72', '70', 'v', '0', '否', '2');
INSERT INTO `sys_dict` VALUES ('73', '13', 'v', 'relation', '关联', '9');
INSERT INTO `sys_dict` VALUES ('74', '0', 'd', 'jdbc_type', '数据库类型', '4');
INSERT INTO `sys_dict` VALUES ('75', '74', 'v', 'mysql', 'MySQL', '1');
INSERT INTO `sys_dict` VALUES ('76', '74', 'v', 'sqlserver', 'SQL Server', '2');
INSERT INTO `sys_dict` VALUES ('77', '74', 'v', 'oracle', 'ORACLE', '3');
INSERT INTO `sys_dict` VALUES ('78', '74', 'v', 'pg', 'PostgreSQL', '4');
INSERT INTO `sys_dict` VALUES ('79', '74', 'v', 'mongodb', 'MongoDB', '5');
INSERT INTO `sys_dict` VALUES ('80', '61', 'v', 'unique', '唯一', '5');

-- ----------------------------
-- Table structure for sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `name` varchar(100) NOT NULL COMMENT '文件名',
  `exts` varchar(10) NOT NULL COMMENT '扩展名',
  `md5` varchar(50) NOT NULL COMMENT '唯一标识',
  `path` varchar(200) NOT NULL COMMENT '保存路径',
  `upload_user_id` int(11) NOT NULL COMMENT '上传用户',
  `upload_time` datetime DEFAULT NULL COMMENT '上传时间',
  `size` int(11) DEFAULT NULL COMMENT '文件大小',
  `cover` varchar(200) DEFAULT NULL COMMENT '封面',
  `length` int(11) DEFAULT NULL COMMENT '长度',
  `pre_file` varchar(200) DEFAULT NULL COMMENT '预览文件',
  `status` char(1) NOT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_file
-- ----------------------------

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `user_id` int(11) NOT NULL COMMENT '操作用户',
  `action_id` int(11) NOT NULL COMMENT '操作动作',
  `action_time` datetime NOT NULL COMMENT '上传时间',
  `param` text COMMENT '参数',
  `ip` varchar(50) NOT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES ('3', '0', '1', '2020-04-24 18:10:57', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('4', '0', '1', '2020-04-24 21:00:41', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('5', '0', '1', '2020-04-24 21:13:48', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('6', '0', '1', '2020-04-24 21:31:48', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('7', '0', '1', '2020-04-26 20:50:30', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('8', '0', '1', '2020-04-26 21:43:59', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('9', '0', '1', '2020-04-27 09:19:42', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('10', '0', '1', '2020-04-27 15:05:02', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('11', '0', '1', '2020-04-27 15:38:35', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('12', '0', '1', '2020-04-27 15:44:00', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('13', '0', '1', '2020-04-27 15:47:48', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('14', '0', '1', '2020-04-27 15:50:00', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('15', '0', '1', '2020-04-27 15:51:37', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('16', '0', '1', '2020-04-27 15:55:37', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('17', '0', '1', '2020-04-27 16:42:46', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('18', '0', '1', '2020-04-27 16:49:18', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('19', '0', '1', '2020-04-27 21:18:51', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('20', '0', '1', '2020-04-27 21:48:57', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('21', '0', '1', '2020-04-28 11:18:50', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('22', '0', '1', '2020-04-28 21:57:24', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('23', '0', '1', '2020-04-29 14:54:59', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('24', '0', '1', '2020-04-29 14:55:02', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('25', '0', '1', '2020-04-29 16:54:15', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('26', '0', '1', '2020-04-29 17:29:23', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('27', '0', '1', '2020-04-29 17:44:56', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('28', '0', '1', '2020-04-29 17:48:47', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('29', '0', '1', '2020-04-29 17:50:24', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('30', '0', '1', '2020-04-29 20:17:43', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('31', '0', '1', '2020-04-29 20:32:52', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('32', '0', '1', '2020-04-29 20:39:25', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('33', '0', '1', '2020-04-29 21:39:45', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('34', '0', '1', '2020-04-30 17:37:14', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('35', '0', '1', '2020-04-30 18:26:40', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('36', '0', '1', '2020-04-30 21:54:32', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('37', '0', '1', '2020-05-01 10:43:50', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('38', '0', '1', '2020-05-01 11:26:33', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('39', '0', '1', '2020-05-01 11:32:57', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('40', '0', '1', '2020-05-01 15:03:31', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('41', '0', '1', '2020-05-02 12:03:42', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('42', '0', '1', '2020-05-02 12:03:46', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('43', '0', '1', '2020-05-02 13:57:18', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('44', '0', '1', '2020-05-02 17:00:10', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('45', '0', '1', '2020-05-02 21:17:48', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('46', '0', '1', '2020-05-02 21:54:39', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('47', '0', '1', '2020-05-03 16:30:15', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('48', '0', '1', '2020-05-03 18:44:12', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('49', '0', '1', '2020-05-03 18:49:56', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('50', '0', '1', '2020-05-05 21:17:27', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('51', '0', '1', '2020-05-06 20:54:36', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('52', '0', '1', '2020-05-06 21:24:30', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('53', '0', '1', '2020-05-06 21:56:16', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('54', '0', '1', '2020-05-07 10:08:04', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('55', '0', '1', '2020-05-07 11:24:43', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('56', '0', '1', '2020-05-07 16:13:08', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('57', '0', '1', '2020-05-07 20:33:52', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('58', '0', '1', '2020-05-07 20:33:57', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');
INSERT INTO `sys_log` VALUES ('59', '0', '1', '2020-05-23 22:15:43', '{\"password\":\"123456\",\"captcha\":\"1234\",\"account\":\"admin\"}', '127.0.0.1');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `app_id` int(11) NOT NULL COMMENT '所属应用',
  `pid` int(11) NOT NULL COMMENT '上级菜单',
  `type` char(1) NOT NULL COMMENT '菜单类型',
  `url` varchar(200) NOT NULL COMMENT '操作地址',
  `title` varchar(50) NOT NULL COMMENT '菜单名称',
  `list_sort` int(11) NOT NULL COMMENT '排序',
  `icon` varchar(20) DEFAULT NULL COMMENT '图标',
  `status` char(1) DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '1', '11', '1', 'dev/module/list.html', '模块管理', '9', null, '1');
INSERT INTO `sys_menu` VALUES ('2', '1', '11', '1', 'dev/models/list.html', '模型管理', '9', null, '1');
INSERT INTO `sys_menu` VALUES ('3', '1', '11', '1', 'dev/field/list.html', '字段管理', '9', null, '1');
INSERT INTO `sys_menu` VALUES ('4', '1', '11', '1', 'dev/validation/list.html', '数据验证', '9', null, '1');
INSERT INTO `sys_menu` VALUES ('5', '2', '12', '1', 'sys/application/list.html', '应用管理', '9', null, '1');
INSERT INTO `sys_menu` VALUES ('6', '2', '12', '1', 'sys/dict/list.html', '字典管理', '9', null, '1');
INSERT INTO `sys_menu` VALUES ('7', '2', '12', '1', 'sys/config/list.html', '配置管理', '9', null, '1');
INSERT INTO `sys_menu` VALUES ('8', '2', '12', '1', 'sys/menu/list.html', '菜单管理', '9', null, '1');
INSERT INTO `sys_menu` VALUES ('9', '2', '24', '1', 'sys/user/list.html', '用户管理', '9', null, '1');
INSERT INTO `sys_menu` VALUES ('10', '2', '24', '1', 'sys/role/list.html', '角色管理', '9', null, '1');
INSERT INTO `sys_menu` VALUES ('11', '1', '0', '0', '#', '开发工具', '1', null, '1');
INSERT INTO `sys_menu` VALUES ('12', '2', '0', '0', '#', '系统管理', '1', null, '1');
INSERT INTO `sys_menu` VALUES ('13', '1', '30', '1', 'dev/database/query', '数据查询', '4', 'database', '1');
INSERT INTO `sys_menu` VALUES ('14', '2', '29', '1', 'sys/token/list.html', '令牌管理', '9', null, '1');
INSERT INTO `sys_menu` VALUES ('15', '2', '29', '1', 'sys/category/list.html', '分类管理', '9', null, '1');
INSERT INTO `sys_menu` VALUES ('16', '2', '29', '1', 'sys/file/list.html', '文件管理', '9', null, '1');
INSERT INTO `sys_menu` VALUES ('17', '2', '29', '1', 'sys/log/list.html', '日志管理', '9', null, '1');
INSERT INTO `sys_menu` VALUES ('18', '2', '12', '1', 'sys/action/list.html', '行为管理', '9', null, '1');
INSERT INTO `sys_menu` VALUES ('19', '2', '24', '1', 'sys/department/list.html', '部门管理', '9', null, '1');
INSERT INTO `sys_menu` VALUES ('20', '1', '30', '1', 'dev/dataset/list.html', '数据集', '9', null, '1');
INSERT INTO `sys_menu` VALUES ('21', '1', '30', '1', 'dev/chart/list.html', '报表管理', '9', null, '1');
INSERT INTO `sys_menu` VALUES ('23', '2', '29', '1', 'sys/notice/list.html', '公告管理', '9', null, '1');
INSERT INTO `sys_menu` VALUES ('24', '2', '0', '0', '#', '用户权限', '2', null, '1');
INSERT INTO `sys_menu` VALUES ('29', '2', '0', '0', '#', '运维管理', '0', null, '1');
INSERT INTO `sys_menu` VALUES ('30', '1', '0', '0', '#', '数据报表', '2', null, '1');
INSERT INTO `sys_menu` VALUES ('31', '2', '29', '1', 'sys/config/set', '系统配置', '4', 'cogs', '1');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `title` varchar(100) NOT NULL COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `add_time` datetime NOT NULL COMMENT '发布时间',
  `add_user_id` int(11) NOT NULL COMMENT '发布人',
  `read_count` int(11) NOT NULL COMMENT '阅读量',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------

-- ----------------------------
-- Table structure for sys_notice_read
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice_read`;
CREATE TABLE `sys_notice_read` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `user_id` int(11) NOT NULL COMMENT '用户',
  `notice_id` int(11) NOT NULL COMMENT '公告',
  `read_time` datetime NOT NULL COMMENT '发布时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_notice_read
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `code` varchar(20) NOT NULL COMMENT '代码',
  `title` varchar(50) NOT NULL COMMENT '名称',
  `is_default` char(1) DEFAULT '0' COMMENT '是否默认',
  `status` char(1) DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `role_id` int(11) NOT NULL COMMENT '角色',
  `menu_id` int(11) NOT NULL COMMENT '菜单',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------

-- ----------------------------
-- Table structure for sys_token
-- ----------------------------
DROP TABLE IF EXISTS `sys_token`;
CREATE TABLE `sys_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `code` varchar(50) NOT NULL COMMENT '代码',
  `user_id` int(11) DEFAULT '0' COMMENT '用户',
  `client_id` int(11) DEFAULT '0' COMMENT '应用',
  `ip` varchar(50) DEFAULT NULL COMMENT 'IP地址',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `expire_time` datetime DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_token
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `account` varchar(50) NOT NULL COMMENT '账号',
  `password` char(32) NOT NULL COMMENT '密码',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(20) DEFAULT NULL COMMENT '手机号',
  `nickname` varchar(100) DEFAULT NULL COMMENT '姓名',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `depart_id` int(11) DEFAULT '9' COMMENT '所在部门',
  `salt` char(6) NOT NULL COMMENT '盐值',
  `status` char(1) NOT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', 'ab711479b07a51ec99b31daec72225b8', '18555788854@qq.com', '312414', '管理员', '2020-03-16 20:39:42', '1', '321445', '1');
INSERT INTO `sys_user` VALUES ('2', 'test', 'a2991761c6be0a10c4d62e2d0deb406e', 'test@qq.com', '13898745214', '演示帐号2', '2020-03-16 20:50:12', '2', '245245', '1');
INSERT INTO `sys_user` VALUES ('3', 'test2', 'db96f43b75aa45b6f8c117264ba660f8', 'test2@qq.com', '15623591545', 'test2', '2020-04-21 15:24:50', '1', '1gks3u', '1');

-- ----------------------------
-- Table structure for sys_user_login
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_login`;
CREATE TABLE `sys_user_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `account` varchar(50) NOT NULL COMMENT '账号',
  `user_id` int(11) DEFAULT '0' COMMENT '用户',
  `ip` varchar(50) DEFAULT NULL COMMENT 'IP地址',
  `session_id` varchar(50) DEFAULT NULL COMMENT 'Session',
  `login_time` datetime DEFAULT NULL COMMENT '登录时间',
  `logout_time` datetime DEFAULT NULL COMMENT '退出时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user_login
-- ----------------------------
INSERT INTO `sys_user_login` VALUES ('1', 'admin', '1', '127.0.0.1', 'node0t2ce1jo5epkf1pvxa2asz6s8n0', '2020-04-01 17:25:34', null);
INSERT INTO `sys_user_login` VALUES ('2', 'admin', '1', '127.0.0.1', 'node0g6jcnynclh4o1agfygitjxi9x0', '2020-04-01 17:29:00', null);
INSERT INTO `sys_user_login` VALUES ('3', 'admin', '1', '127.0.0.1', 'node0140p7qiy74i71kb7d1cjm3rq70', '2020-04-01 18:00:12', null);
INSERT INTO `sys_user_login` VALUES ('4', 'admin', '1', '127.0.0.1', 'node0o7l2x111fcjc1bqy0wm87lpeg0', '2020-04-01 18:08:42', null);
INSERT INTO `sys_user_login` VALUES ('5', 'admin', '1', '127.0.0.1', 'node01t5x9ofp4mxqmxs39u9pednkd0', '2020-04-02 10:56:11', null);
INSERT INTO `sys_user_login` VALUES ('6', 'admin', '1', '127.0.0.1', 'node0oeyo1dozcvrv66muntwee8ur0', '2020-04-02 13:14:03', null);
INSERT INTO `sys_user_login` VALUES ('7', 'admin', '1', '127.0.0.1', 'node01evexr600i6oq6h9l5jh4nhzc0', '2020-04-02 13:17:22', null);
INSERT INTO `sys_user_login` VALUES ('8', 'admin', '1', '127.0.0.1', 'node016zzut4jxhw343k2pbriqhxbk0', '2020-04-02 13:49:12', null);
INSERT INTO `sys_user_login` VALUES ('9', 'admin', '1', '127.0.0.1', 'node01af8h98w7dmar1e6ssh5vpodvr0', '2020-04-02 15:16:29', null);
INSERT INTO `sys_user_login` VALUES ('10', 'admin', '1', '127.0.0.1', 'node01fpyjfkd6z1bsx8u9arj3ahi21', '2020-04-02 16:04:00', null);
INSERT INTO `sys_user_login` VALUES ('11', 'admin', '1', '127.0.0.1', 'node01vjc4r2q02fumn41u0dve15hm0', '2020-04-02 16:07:28', null);
INSERT INTO `sys_user_login` VALUES ('12', 'admin', '1', '127.0.0.1', 'node016bzf3vaqde31wbrl55yfkbmo0', '2020-04-02 21:41:30', null);
INSERT INTO `sys_user_login` VALUES ('13', 'admin', '1', '127.0.0.1', 'node0bfgs5zho5xph12xqy4spdm9oc0', '2020-04-02 21:47:11', null);
INSERT INTO `sys_user_login` VALUES ('14', 'admin', '1', '127.0.0.1', 'node0z1m1zjevo7pd9aovtn1fb3n40', '2020-04-03 16:43:32', null);
INSERT INTO `sys_user_login` VALUES ('15', 'admin', '1', '127.0.0.1', 'node0oyajob2930er1b28dv9nfwqb20', '2020-04-03 17:12:01', null);
INSERT INTO `sys_user_login` VALUES ('16', 'admin', '1', '127.0.0.1', 'node0zm4mgy67ifcw1fdd9msmtaoaf1', '2020-04-03 21:23:50', null);
INSERT INTO `sys_user_login` VALUES ('17', 'admin', '1', '127.0.0.1', 'node03xlr8sh2lg471n1t7yjcjp0wo0', '2020-04-03 21:29:25', null);
INSERT INTO `sys_user_login` VALUES ('18', 'admin', '1', '127.0.0.1', 'node01r296g9vxiys1tjsammnz2h1x0', '2020-04-03 21:36:20', null);
INSERT INTO `sys_user_login` VALUES ('19', 'admin', '1', '127.0.0.1', 'node095ngw874sr7i1cqauauxj6zts0', '2020-04-04 17:54:48', null);
INSERT INTO `sys_user_login` VALUES ('20', 'admin', '1', '127.0.0.1', 'node0d61og8tmywzu1fn3i3yydx3yt0', '2020-04-08 15:31:46', null);
INSERT INTO `sys_user_login` VALUES ('21', 'admin', '1', '127.0.0.1', 'node01uroadsoy98yp14xyncmpoqnsl0', '2020-04-08 17:34:35', null);
INSERT INTO `sys_user_login` VALUES ('22', 'admin', '1', '127.0.0.1', 'node01sa8n44zdtaf71tw0k6cre0u540', '2020-04-08 17:50:05', null);
INSERT INTO `sys_user_login` VALUES ('23', 'admin', '1', '127.0.0.1', 'node01cpkjrbr5v403syouwpf3upss0', '2020-04-08 17:55:00', null);
INSERT INTO `sys_user_login` VALUES ('24', 'admin', '1', '127.0.0.1', 'node0n1e4ckhasbnxfhupo8yxi7a50', '2020-04-08 17:57:29', null);
INSERT INTO `sys_user_login` VALUES ('25', 'admin', '1', '127.0.0.1', 'node013v1rtg0xcjvx1djel7w0lp1ty0', '2020-04-08 17:59:10', null);
INSERT INTO `sys_user_login` VALUES ('26', 'admin', '1', '127.0.0.1', 'node01e4j6sx52hheitfn6u8usk2dr0', '2020-04-08 21:23:47', null);
INSERT INTO `sys_user_login` VALUES ('27', 'admin', '1', '127.0.0.1', 'node0exaeq25dt7vjjtg46n3am5pu0', '2020-04-08 21:26:29', null);
INSERT INTO `sys_user_login` VALUES ('28', 'admin', '1', '127.0.0.1', 'node013hp6mm6y40ch117vlvcidcgme0', '2020-04-09 16:14:04', null);
INSERT INTO `sys_user_login` VALUES ('29', 'admin', '1', '127.0.0.1', 'node01msxuwol4bigbrop9cvhdv72c0', '2020-04-09 16:27:14', null);
INSERT INTO `sys_user_login` VALUES ('30', 'admin', '1', '127.0.0.1', 'node01msxuwol4bigbrop9cvhdv72c0', '2020-04-09 16:27:14', null);
INSERT INTO `sys_user_login` VALUES ('31', 'admin', '1', '127.0.0.1', 'node01dn3secw7n1c71eae0mutgtj2n1', '2020-04-09 21:10:25', null);
INSERT INTO `sys_user_login` VALUES ('32', 'admin', '1', '127.0.0.1', 'node01dn3secw7n1c71eae0mutgtj2n1', '2020-04-09 21:10:27', null);
INSERT INTO `sys_user_login` VALUES ('33', 'admin', '1', '127.0.0.1', 'node01cdyo9pztwiv087tahtoorlpn0', '2020-04-10 10:06:42', null);
INSERT INTO `sys_user_login` VALUES ('34', 'admin', '1', '127.0.0.1', 'node0htsvcim8jjpo1d5c6q4nksioz1', '2020-04-10 13:15:11', null);
INSERT INTO `sys_user_login` VALUES ('35', 'admin', '1', '127.0.0.1', 'node0eivfvyjwvw0h1qvbj4mhae0412', '2020-04-10 15:48:04', null);
INSERT INTO `sys_user_login` VALUES ('36', 'admin', '1', '127.0.0.1', 'node01fa7uxymw87mv1ewhwzjfpw9yk0', '2020-04-10 16:42:55', null);
INSERT INTO `sys_user_login` VALUES ('37', 'admin', '1', '127.0.0.1', 'node011eddkbzuh326viidqg8o6z720', '2020-04-10 16:46:34', null);
INSERT INTO `sys_user_login` VALUES ('38', 'admin', '1', '127.0.0.1', 'node019ruggfvo9f1ai4vdmmtutbu90', '2020-04-21 15:21:58', null);
INSERT INTO `sys_user_login` VALUES ('39', 'admin', '1', '127.0.0.1', 'node0eisojozantaxg262cv9igy261', '2020-04-21 16:11:04', null);
INSERT INTO `sys_user_login` VALUES ('40', 'admin', '1', '127.0.0.1', 'node01jx9sirewrtd41oefkcphwwpcs0', '2020-04-24 14:58:45', null);
INSERT INTO `sys_user_login` VALUES ('41', 'admin', '1', '127.0.0.1', 'node01klsspa1n207w1wv58x9pdqxxs0', '2020-04-24 16:07:57', null);
INSERT INTO `sys_user_login` VALUES ('42', 'admin', '1', '127.0.0.1', 'node0gpcof7ql8g501v3foj42esw5b0', '2020-04-24 16:12:22', null);
INSERT INTO `sys_user_login` VALUES ('43', 'admin', '1', '127.0.0.1', 'node01m1mkxlngmy5t1hjag8j8uxjkh0', '2020-04-24 16:20:16', null);
INSERT INTO `sys_user_login` VALUES ('44', 'admin', '1', '127.0.0.1', 'node01fsz762dc7ei71gq8ifqgpqnaz0', '2020-04-24 16:23:17', null);
INSERT INTO `sys_user_login` VALUES ('45', 'admin', '1', '127.0.0.1', 'node09eh6vjplzoqo6nw5m37je9p70', '2020-04-24 16:43:10', null);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增长主键ID',
  `user_id` int(11) NOT NULL COMMENT '用户',
  `role_id` int(11) NOT NULL COMMENT '角色',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
