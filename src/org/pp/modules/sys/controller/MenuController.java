package org.pp.modules.sys.controller;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Kv;

import java.util.List;

import org.pp.core.AdminController;
import org.pp.core.BaseService;
import org.pp.core.RestObject;
import org.pp.modules.sys.model.Menu;
import org.pp.modules.sys.service.MenuService;
import org.pp.modules.sys.vo.Tree;

public class MenuController extends AdminController<Menu> {

	@Inject
	MenuService service;
	
	/**
	 * 上级分组
	 */
	public void groups() {
		List<Tree> tree = service.groups(0l);
		if(tree == null) renderJson(RestObject.success(null));
		else renderJson(RestObject.success(tree.size(),tree));
	}
		
	/**
	 * 菜单树形
	 */
	public void tree() {
		List<Tree> tree = service.tree(0l);
		renderJson(RestObject.success(tree.size(),tree));
	}
	
	@Override
	protected Kv search() {
		Kv kv= Kv.create();
		if(!isParaBlank("pid")) {
			kv.put("pid = ", getParaToLong("pid"));
		}		
		if(!isParaBlank("appId")) {
			kv.put("app_id = ", getParaToLong("appId"));
		}	
		if(!isParaBlank("title")) {
			kv.put("title like ", "%"+getPara("title")+"%");
		}		
		if(!isParaBlank("type")) {
			kv.put("type = ", getPara("type"));
		}		
		if(!isParaBlank("status")) {
			kv.put("status = ", getPara("appId"));
		}			
		return kv;
	}
	
	@Override
	protected BaseService<Menu> getService() {
		return service;
	}
}
