package org.pp.modules.sys.controller;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Kv;

import org.pp.core.AdminController;
import org.pp.core.BaseService;
import org.pp.modules.sys.model.File;
import org.pp.modules.sys.service.FileService;

public class FileController extends AdminController<File> {

	@Inject
	FileService service;
	
	
	@Override
	protected Kv search() {
		Kv kv= Kv.create();
		if(!isParaBlank("uploadUserId")) {
			kv.put("upload_user_id = ", getParaToLong("uploadUserId"));
		}		
		if(!isParaBlank("exts")) {
			kv.put("exts = ", getParaToLong("exts"));
		}		
		if(!isParaBlank("status")) {
			kv.put("status = ", getParaToLong("status"));
		}	
		if(!isParaBlank("name")) {
			kv.put("name like ", "%"+getPara("name")+"%");
		}	
		if(!isParaBlank("startTime")) {
			kv.put("upload_time > ", getPara("startTime"));
		}	
		if(!isParaBlank("endTime")) {
			kv.put("upload_time > ", getPara("endTime"));
		}	
		return kv;
	}
	
	@Override
	protected BaseService<File> getService() {
		return service;
	}

}
