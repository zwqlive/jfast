package org.pp.modules.sys.vo;

import java.io.Serializable;

@SuppressWarnings("serial")
public class UserInfo implements Serializable{
	private long id;
	private String account;
	private String nickname;
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
}
