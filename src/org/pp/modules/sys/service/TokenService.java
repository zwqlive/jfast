package org.pp.modules.sys.service;

import org.pp.core.BaseService;
import org.pp.modules.sys.model.Token;

public class TokenService implements BaseService<Token>{
	
	public static Token dao = new Token().dao();

	private String error = "";
	@Override
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	@Override
	public Token getModel() {
		return dao;
	}
}
