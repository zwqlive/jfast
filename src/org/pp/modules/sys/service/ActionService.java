package org.pp.modules.sys.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.pp.core.BaseService;
import org.pp.modules.sys.model.Action;

public class ActionService implements BaseService<Action>{
	
	public static Action dao = new Action().dao();
	private static Map<String,Action> maps;
	private String error = "";
	
	/**
	 * 将全部action映射成map，主键为ID或URL
	 * @return
	 */
	public static Map<String,Action> map(){
		if(maps == null || maps.size() == 0) {
			List<Action> list = dao.findAll();
			maps = new HashMap<>();
			for(Action a : list) {
				maps.put(a.getUrl(), a);
				maps.put(a.getId()+"", a);
			}
		}		
		return maps;
	}
	
	@Override
	public String getError() {
		return error;
	}

	@Override
	public void setError(String error) {
		this.error = error;
	}
	@Override
	public Action getModel() {
		return dao;
	}
}
