package org.pp.utils;

import javax.servlet.http.HttpServletRequest;

public class HttpUtil {
	public static boolean isMethod(HttpServletRequest request, String method) {
		return request.getMethod().equalsIgnoreCase(method);
	}
	public static boolean isGet(HttpServletRequest request) {
		return "get".equalsIgnoreCase(request.getMethod());
	}
	public static boolean isPost(HttpServletRequest request) {
		return "post".equalsIgnoreCase(request.getMethod());
	}
	
	/***
     * 判断一个请求是否为AJAX请求,是则返回true
     * @param request HttpServletRequest
     * @return
     */
    public static boolean isAjax(HttpServletRequest request) {
    	String requestType = request.getHeader("X-Requested-With"); 
    	if(requestType == null){
    		return false;
    	}
		return true;
    }
}
