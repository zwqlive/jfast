package org.pp.interceptor;

import org.pp.consts.RestConst;
import org.pp.core.RestObject;
import org.pp.modules.sys.model.User;
import org.pp.modules.sys.service.LogService;
import org.pp.utils.HttpUtil;
import org.pp.utils.SecurityUtil;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;

/**
 * 后台拦截器
 * 1、强制用户登录，如果未登录，强制跳转到登录页面
 * 2、判断是否有访问权限，如果没有，强制跳转到登录页面
 * 3、记录操作日志
 * @author Administrator
 *
 */
public class BackendInterceptor implements Interceptor{

	@Override
	public void intercept(Invocation inv) {
		Controller c = inv.getController();
		Object user = c.getSessionAttr("user");
		String access = c.getRequest().getRequestURI();
		boolean isAjax = HttpUtil.isAjax(c.getRequest());
		if(user != null) {
			SecurityUtil.setUser((User)user);
			c.setAttr("_user", user);
			if(SecurityUtil.checkAccess(access)) {
				inv.invoke();
			}else {
				if(isAjax) {
					c.renderJson(RestObject.error(RestConst.DENY, "您没有权限", SecurityUtil.loginUrl()));
				}else {
					c.setSessionAttr("redirect", access);
					c.set("redirect", access);
					c.render("/view/public/login.html");
				}
			}
			SecurityUtil.releaseUser();
		}else {
			if(isAjax) {
				c.renderJson(RestObject.error(RestConst.NO_AUTH, "您还没有登录请重新登录", SecurityUtil.loginUrl()));
			}else {
				c.setSessionAttr("redirect", access);
				c.set("redirect", access);
				c.render("/view/public/login.html");
			}
		}		
	}
}
