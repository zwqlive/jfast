package org.pp.interceptor;

import org.pp.core.RestObject;
import org.pp.modules.sys.service.LogService;
import org.pp.utils.HttpUtil;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
public class ExceptionInterceptor implements Interceptor{

	@Override
	public void intercept(Invocation inv) {
		try {
			LogService.log(inv.getController().getRequest());
			inv.invoke();
		}catch (Exception e) {
			e.printStackTrace();
			Controller c = inv.getController();
			if(HttpUtil.isAjax(c.getRequest())) {
				c.renderJson(RestObject.error("9", e.getMessage(),""));
			}else {
				c.renderError(500);
			}
		}
		
	}

}
