package org.pp.controller;

import org.pp.core.BaseController;
import org.pp.modules.sys.service.ConfigService;

public class HtmlController extends BaseController {

	public void index() {
		parseRoute(getAttrForStr("_target"));
		
		String module = getAttrForStr("_module");
		String controller = getAttrForStr("_controller");
		String action = getAttrForStr("_action");
		String view = "/view/"+module+"/"+controller+"/";
		if("edit".equalsIgnoreCase(action)) {
			view += "form.html";
		}else if("add".equalsIgnoreCase(action)) {
			view += "form.html";
		}else if("imports".equalsIgnoreCase(action)) {
			view = "/view/public/imports.html";
		}else {
			view += action+".html";
		}
		this.setAttr("_config", ConfigService.map());
		keepPara();
		render(view);
	}

	public void add() {

	}

	public void edit() {

	}

	public void imports() {

	}
}
